import java.util.Scanner;

public class Array11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[5];
        System.out.println("Array");
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);

        }
        while (true) {
            for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i] + " ");

            }
            if (arr[0] <= arr[1] && arr[1] <= arr[2] && arr[2] <= arr[3] && arr[3] <= arr[4]) {
                System.out.println();
                System.out.println("You win!!!");
                break;
            }
            System.out.println();
            System.out.print("Please input index: ");
            int index1 = sc.nextInt();
            int index2 = sc.nextInt();
            int temp = arr[index1];
            arr[index1] = arr[index2];
            arr[index2] = temp;

        }

        sc.close();
    }
}
