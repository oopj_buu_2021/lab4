import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        int unisize = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        int size = sc.nextInt();
        int arr[] = new int[size];
        int uniqe[] = new int[size];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Element " + i + ": ");
            arr[i] = sc.nextInt();
            int index = -1;
            for (int j = 0; j < unisize; j++) {
                if (uniqe[j] == arr[i]) {
                    index = j;
                }

            }
            if (index < 0) {
                uniqe[unisize] = arr[i];
                unisize++;
            }

        }
        System.out.print("All number: ");
        for (int i = 0; i < unisize; i++) {
            System.out.print(uniqe[i]+" ");
        }

        sc.close();
    }

}
