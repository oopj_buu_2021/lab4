import java.util.Scanner;

public class Array8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
            sum = sum + arr[i];

        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");

        }
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
            }
        }
        System.out.println();
        System.out.println("sum = " + sum);
        System.out.println("avg = " + (sum / arr.length));
        System.out.println("max = " + max);

        sc.close();
    }

}
