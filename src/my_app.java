import java.util.Scanner;

public class my_app {
    // ผมตั้งชื่อว่า myapp ตามอาจารณ์แล้วมันขึ้นแดง ผมเลยใส่ _ ไว้นะครับ
    static void Welcome() {
        System.out.println("Welcome to my app!!!");
    }

    static void Exit() {
        System.exit(0);
        System.out.println("Bye!!");
    }

    static void Menu() {
        System.out.println("--Menu--");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }

    static int Inputchoice() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input your choice(1-3): ");
            int choice = sc.nextInt();
            if (choice >= 1 && choice <= 3) {
                return choice;
            }
            System.out.println("Error: Please input between 1-3");
        }

    }

    static void Helloworld() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input time:");
        int Time = sc.nextInt();
        for (int i = 0; i < Time; i++) {
            System.out.println("Hello World!!!");
        }
        
    }

    static void Add() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number:");
        int First = sc.nextInt();
        System.out.print("Please input second number:");
        int Second = sc.nextInt();
        System.out.println("Result = " + (First + Second));
        
    }

    public static void main(String[] args) {
        while (true) {
            Welcome();
            Menu();
            int choice = Inputchoice();
            switch (choice) {
                case 1:
                    Helloworld();
                    break;
                case 2:
                    Add();
                    break;
                case 3:
                    Exit();
                    break;

            }
        }

    }
}
